package org.alse.befun.controller

import org.alse.befun.MessageProducer
import org.alse.befun.SnsMessageProducer
import org.alse.befun.dao.Rating
import org.alse.befun.dao.RatingRepository
import org.apache.commons.io.IOUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import java.io.InputStream
import java.util.*


@RestController
class MainController(private val messageProducer: MessageProducer,
                     private val ratingRepository: RatingRepository,
                     private val snsMessageProducer: SnsMessageProducer) {

    @Value("\${img.url}")
    private lateinit var imgUrl: String

    @GetMapping
    fun getAll() = ratingRepository.findAll().joinToString(separator = "<br/>") {
        "<a href=\"${it.id}\">$it</a>"
    } + "<br/> or your can voting <a href=\"/random\">random</a>"

    @GetMapping("{uuid}")
    fun putMessage(@PathVariable uuid: UUID): String {
        messageProducer.sendMessage(uuid)
        return "Popularity increment requested! <br/> <a href='/'>Back</a>"
    }

    @GetMapping("random")
    fun random(): String {
        snsMessageProducer.send(ratingRepository.findAll().map(Rating::id), "random")
        return "Requested random popularity increment! <br/> <a href='/'>Back</a>"
    }

    @GetMapping(value = ["/image"], produces = [MediaType.IMAGE_JPEG_VALUE])
    @ResponseBody
    fun getImageWithMediaType(): ByteArray {
        val img: InputStream = javaClass.getResourceAsStream("/static/big.jpg")
        return IOUtils.toByteArray(img)
    }

    @GetMapping("cached")
    fun cachedImage() = "<img src=\"$imgUrl\"/>"

    @GetMapping("/er")
    fun error() {
        throw Exception("Wow, Hello")
    }
}
