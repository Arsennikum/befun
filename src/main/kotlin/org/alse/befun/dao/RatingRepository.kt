package org.alse.befun.dao

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface RatingRepository: JpaRepository<Rating, UUID>
