package org.alse.befun

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BefunApplication

fun main(args: Array<String>) {
	runApplication<BefunApplication>(*args)
}

