package org.alse.befun.config

import com.amazonaws.services.sns.AmazonSNS
import com.amazonaws.services.sqs.AmazonSQSAsync
import org.springframework.cloud.aws.messaging.core.NotificationMessagingTemplate
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SpringCloudAwsConfig {
    @Bean
    fun queueMessagingTemplate(amazonSQSAsync: AmazonSQSAsync) = QueueMessagingTemplate(amazonSQSAsync)

    @Bean
    fun notificationMessagingTemplate(amazonSNS: AmazonSNS) = NotificationMessagingTemplate(amazonSNS)
}
