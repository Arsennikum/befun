package org.alse.befun

import org.slf4j.LoggerFactory
import org.springframework.cloud.aws.messaging.core.NotificationMessagingTemplate
import org.springframework.stereotype.Service

@Service
class SnsMessageProducer(val messagingTemplate: NotificationMessagingTemplate) {
    private val log = LoggerFactory.getLogger(this.javaClass)

    fun send(msg: Any, subject: String) {
        log.info("Sending to SNS queue message $msg with subject $subject")
        messagingTemplate.sendNotification(QueueSetting.SNS_NAME, msg, subject);
    }
}
