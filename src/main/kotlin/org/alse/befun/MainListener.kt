package org.alse.befun

import org.alse.befun.services.RatingService
import org.slf4j.LoggerFactory
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener
import org.springframework.messaging.handler.annotation.Header
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Service
import java.util.*

@Service
class MainListener(val ratingService: RatingService) {
    private val log = LoggerFactory.getLogger(this.javaClass)

    @SqsListener(QueueSetting.NAME)
    fun listen(@Payload uuid: UUID, @Header("SenderId") senderId: String) {
        log.info("message receive: $uuid (from sender: $senderId)")
        ratingService.updateRating(uuid)
    }

}
