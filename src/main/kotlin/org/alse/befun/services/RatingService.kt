package org.alse.befun.services

import org.alse.befun.dao.RatingRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class RatingService(private val ratingRepository: RatingRepository) {

    @Transactional
    fun updateRating(id: UUID) {
        ratingRepository.getOne(id).popularity++
    }

}
