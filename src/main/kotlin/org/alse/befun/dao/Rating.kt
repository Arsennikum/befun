package org.alse.befun.dao

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table
data class Rating (
        @Id
        @Column
        val id: UUID,

        @Column
        var popularity: Int,

        @Column
        var description: String?
)
