package org.alse.befun

import org.slf4j.LoggerFactory
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate
import org.springframework.stereotype.Service

@Service
class MessageProducer(private val messagingTemplate: QueueMessagingTemplate) {
    private val log = LoggerFactory.getLogger(this.javaClass)

    fun sendMessage(message: Any) {
        log.info("Sending message...")
        messagingTemplate.convertAndSend(QueueSetting.NAME, message)
    }

}
